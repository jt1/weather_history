from django.views.generic import DetailView, TemplateView

from weather import const
from weather.models import Place, WebCamPlace
from weather.utils import datetime_to_millis

import datetime


class WebCamPlaceListView(TemplateView):
    template_name = 'weather/webcamplace_list.html'

    def get_context_data(self, **kwargs):
        context = super(WebCamPlaceListView, self).get_context_data(**kwargs)
        date = self.request.GET.get('date')
        date = datetime.datetime.strptime(date, '%Y-%m-%d') if date else datetime.datetime.today().date()
        if datetime.datetime.now().hour < const.SUNRISE_HOUR:
            date = date - datetime.timedelta(days=1)
        context['date'] = date
        context['places'] = {place: place.webcamimage_set.filter(date__gte=date, date__lte=date + datetime.timedelta(days=1)).order_by('date') for place in WebCamPlace.objects.all()}
        return context


class PlaceListView(TemplateView):
    template_name = 'weather/index.html'


class PlaceDetailView(DetailView):
    model = Place

    def get_context_data(self, **kwargs):
        context = super(PlaceDetailView, self).get_context_data(**kwargs)
        days = self.request.GET.get('days', '7')
        end_time = self.object.temperature_set.latest('date').date
        start_time = end_time - datetime.timedelta(days=int(days))
        start_time_for_data = end_time - datetime.timedelta(days=int(days) + 2)
        context['days'] = days
        context['start_time'] = datetime_to_millis(start_time)
        context['end_time'] = datetime_to_millis(end_time)

        context['url_temp'] = self.object.url_temp
        context['url_wind'] = self.object.url_wind
        context['url_snow_cover'] = self.object.url_snow_cover
        context['url_showers'] = self.object.url_showers
        context['url_avalanche_risk'] = self.object.url_avalanche_risk
        context['temp_data'] = [[datetime_to_millis(temp.date), temp.value] for temp in self.object.temperature_set.filter(date__gte=start_time_for_data)]
        context['wind_data'] = [[datetime_to_millis(wind.date), wind.avg, wind.angle] for wind in self.object.wind_set.filter(date__gte=start_time)]
        context['avalanche_risk_data'] = [[datetime_to_millis(avalanche.date), avalanche.value] for avalanche in self.object.avalancherisk_set.filter(date__gte=start_time_for_data)]
        context['snow_cover_data'] = [[datetime_to_millis(snow_cover.date), snow_cover.value] for snow_cover in self.object.snowcover_set.filter(date__gte=start_time_for_data)]
        context['snow_showers_data'] = [[datetime_to_millis(shower.date), shower.value] for shower in self.object.showers_set.filter(date__gte=start_time_for_data, is_rain=False)]
        context['rain_showers_data'] = [[datetime_to_millis(shower.date), shower.value] for shower in self.object.showers_set.filter(date__gte=start_time_for_data, is_rain=True)]
        context['clouds'] = self.object.clouds_set.all()[:30]

        context['places'] = Place.objects.all()
        return context
