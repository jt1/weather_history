# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import urllib
from django.core.files import File
from django.db import models
from easy_thumbnails.files import get_thumbnailer


class WebCamPlace(models.Model):
    name = models.CharField("name", max_length=255)
    url = models.URLField("url")

    class Meta:
        ordering = ['name']
        verbose_name = u'WebCam Place'
        verbose_name_plural = u'WebCam Places'

    def update(self):
        filename, headers = urllib.urlretrieve(self.url)
        webcam_image = WebCamImage(place=self)
        webcam_image.image.save(os.path.basename(self.url), File(open(filename)))
        thumbnailer = get_thumbnailer(webcam_image.image)
        thumbnailer.get_thumbnail({'crop': True, 'size': (100, 100)})

    def __unicode__(self):
        return '{}'.format(self.name)


class WebCamImage(models.Model):
    image = models.ImageField(upload_to='images/%Y_%m_%d/')
    date = models.DateTimeField('date', auto_now_add=True, null=True)
    place = models.ForeignKey(WebCamPlace)

    class Meta:
        verbose_name = u'WebCam Image'
        verbose_name_plural = u'WebCam Images'
        ordering = ['-date']

    def __unicode__(self):
        return '{}'.format(self.image)


class Place(models.Model):
    name = models.CharField("name", max_length=255)
    url_temp = models.URLField("url temp")
    url_wind = models.URLField("url wind", null=True, blank=True)
    url_snow_cover = models.URLField("url snow cover", null=True, blank=True)
    url_showers = models.URLField("url showers", null=True, blank=True)
    url_avalanche_risk = models.URLField("url avalanche risk", null=True, blank=True)

    def update(self):
        from weather.weather_source import WeatherSource
        ws = WeatherSource()
        ws.update('temp', self.url_temp, self)
        ws.update('wind', self.url_wind, self)
        ws.update('snow_cover', self.url_snow_cover, self)
        ws.update('showers', self.url_showers, self)
        ws.update('avalanche_risk', self.url_avalanche_risk, self)

    def __unicode__(self):
        return '{}'.format(self.name)


class Temperature(models.Model):
    date = models.DateTimeField('date')
    value = models.FloatField('temp')
    place = models.ForeignKey(Place)

    class Meta:
        verbose_name = u'Temperature'
        verbose_name_plural = u'Temperature'
        ordering = ['-date']

    def __unicode__(self):
        return '{}: {}C'.format(self.date, self.value)


class Showers(models.Model):
    date = models.DateTimeField('date')
    value = models.FloatField('mm')
    is_rain = models.BooleanField('deszcz', default=True)
    place = models.ForeignKey(Place)

    class Meta:
        verbose_name = u'Showers'
        verbose_name_plural = u'Showers'
        ordering = ['-date']

    def __unicode__(self):
        return '{}: {}mm ({})'.format(self.date, self.value, 'deszcz' if self.is_rain else 'śnieg')


class SnowCover(models.Model):
    date = models.DateTimeField('date')
    value = models.IntegerField('snow cover')
    place = models.ForeignKey(Place)

    class Meta:
        verbose_name = u'Snow Cover'
        verbose_name_plural = u'Snow Cover'
        ordering = ['-date']

    def __unicode__(self):
        return '{}: {}mm'.format(self.date, self.value)


class AvalancheRisk(models.Model):
    date = models.DateTimeField('date')
    value = models.FloatField('risk level')
    place = models.ForeignKey(Place)

    class Meta:
        verbose_name = u'Avalanche Risk'
        verbose_name_plural = u'Avalanche Risk'
        ordering = ['-date']

    def __unicode__(self):
        return '{}: {}'.format(self.date, self.value)


class Wind(models.Model):
    date = models.DateTimeField('date')
    avg = models.IntegerField('avg')
    max = models.IntegerField('max')
    angle = models.IntegerField('angle')
    direction = models.CharField('direction', max_length=30)
    place = models.ForeignKey(Place)

    class Meta:
        verbose_name = u'Wind'
        verbose_name_plural = u'Wind'
        ordering = ['-date']

    def __unicode__(self):
        return '{}: {}/{}'.format(self.date, self.avg, self.direction)


class Clouds(models.Model):
    date = models.DateTimeField('date')
    value = models.CharField('clouds', max_length=30)
    place = models.ForeignKey(Place)

    class Meta:
        verbose_name = u'Clouds'
        verbose_name_plural = u'Clouds'
        ordering = ['-date']

    def __unicode__(self):
        return '{}: {}'.format(self.date, self.value)



