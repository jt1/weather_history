# -*- coding: utf-8 -*-
import re
import datetime

import requests
from bs4 import BeautifulSoup
from weather.models import Temperature, Clouds, SnowCover, Showers, Wind, AvalancheRisk


class WeatherSource(object):

    sources = []

    @staticmethod
    def _get_soup(url):
        return BeautifulSoup(requests.get(url).content, 'html.parser')

    def update(self, what, url, place):
        [getattr(source, 'update_' + what)(url, place) for source in self.sources if source.url in url]

    @staticmethod
    def update_temp(url, place):
        pass

    @staticmethod
    def update_snow_cover(url, place):
        pass

    @staticmethod
    def update_showers(url, place):
        pass

    @staticmethod
    def update_wind(url, place):
        pass

    @staticmethod
    def update_avalanche_risk(url, place):
        pass


class WeatherOnline(WeatherSource):

    url = u'www.weatheronline.pl'

    @staticmethod
    def update_temp(url, place):
        for tr in WeatherSource._get_soup(url).find('table', {'class': 'gr1'}).findAll('tr')[1:]:
            tds = tr.find_all('td')
            date = datetime.datetime.strptime(tds[0].text.encode('ascii', 'ignore'), '%d.%m.%Y%H:%M')
            Temperature.objects.update_or_create(place=place, date=date, defaults={'value': float(tds[1].text.strip().split()[0])})
            Clouds.objects.update_or_create(place=place, date=date, defaults={'value': tds[2].text.strip().lower()})

    @staticmethod
    def update_snow_cover(url, place):
        for tr in WeatherSource._get_soup(url).find('table', {'class': 'gr1'}).findAll('tr')[1:]:
            tds = tr.find_all('td')
            date = datetime.datetime.strptime(tds[0].text.encode('ascii', 'ignore'), '%d.%m.%Y%H:%M')
            snow_cover = tds[1].text.strip()
            if '-' not in snow_cover:
                SnowCover.objects.update_or_create(place=place, date=date, defaults={'value': snow_cover.split()[0]})

    @staticmethod
    def update_showers(url, place):
        for tr in WeatherSource._get_soup(url).find('table', {'class': 'gr1'}).findAll('tr')[1:]:
            tds = tr.find_all('td')
            date = datetime.datetime.strptime(tds[0].text.encode('ascii', 'ignore'), '%d.%m.%Y%H:%M')
            text = tds[1].text.strip()
            if text == 'brak komunikatu':
                text = '0 l/m'
            match = re.match(r'.*([0-9\.]+) l/m', text)
            Showers.objects.update_or_create(place=place, date=date, defaults={'value': ''.join(match.groups()), 'is_rain': Temperature.objects.filter(place=place).latest('date').value > 1})

    @staticmethod
    def update_wind(url, place):
        for tr in WeatherSource._get_soup(url).find('table', {'class': 'gr1'}).findAll('tr')[2:]:
            tds = tr.find_all('td')
            date = datetime.datetime.strptime(tds[0].text.encode('ascii', 'ignore'), '%d.%m.%Y%H:%M')
            Wind.objects.update_or_create(place=place,
                                          date=date,
                                          defaults={'avg': int(tds[1].text.strip().replace('-', '0')),
                                                    'max': int(tds[3].text.strip().replace('-', '0')),
                                                    'angle': int(tds[4].text.replace('-', '0-').replace('VRB', '0-').encode('ascii', 'ignore').strip()[:-1]),
                                                    'direction': tds[5].text.encode('ascii', 'ignore').strip(),
                                                    })


class HorskaZachrannaSluzba(WeatherSource):
    url = u'www.hzs.sk'

    @staticmethod
    def update_avalanche_risk(url, place):
        soup = WeatherSource._get_soup(url)
        level = soup.find('div', {'class': 'laviny_1'}).find('img', {'class': 'laviny_pic'}).attrs['src'][42]
        date_text = soup.select('#content > div.cms > div.cms_title > h2')[0].text.split(',')[1].strip()
        date = datetime.datetime.strptime(date_text, '%d.%m.%Y o %H.%M')
        AvalancheRisk.objects.update_or_create(place=place, date=date, defaults={'value': float(level)})


class TOPR(WeatherSource):
    url = u'lawiny.topr.pl'

    @staticmethod
    def update_avalanche_risk(url, place):
        soup = WeatherSource._get_soup(url)

        for script in soup.findAll('script'):
            level = re.search(r'"lev":(\d)', script.text)
            if level:
                date_text = re.search(r'"iat":"(.*?)"', script.text).groups()[0]
                date = datetime.datetime.strptime(date_text, '%Y-%m-%d %H:%M')
                AvalancheRisk.objects.update_or_create(place=place, date=date, defaults={'value': float(level.groups()[0])})
                return

WeatherSource.sources = [WeatherOnline, HorskaZachrannaSluzba, TOPR]
