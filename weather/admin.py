from django.contrib import admin
from weather.models import Temperature, AvalancheRisk, Place, Clouds, Wind, SnowCover, Showers, WebCamImage, WebCamPlace

admin.site.register(Place)
admin.site.register(WebCamImage)
admin.site.register(WebCamPlace)
admin.site.register(Temperature)
admin.site.register(Clouds)
admin.site.register(Wind)
admin.site.register(SnowCover)
admin.site.register(Showers)
admin.site.register(AvalancheRisk)
