# -*- coding: utf-8 -*-

import time


def datetime_to_millis(dt):
    return str(int(time.mktime(dt.timetuple()) * 1000))

