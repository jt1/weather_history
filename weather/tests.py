from django.test import TestCase
from datetime import datetime
from weather.utils import datetime_to_millis


class TestUtils(TestCase):
    def test_datetime_to_millis(self):
        self.assertEqual(datetime_to_millis(datetime(2015, 12, 05, 14, 25, 34)), '1449321934000')