# -*- coding: utf-8 -*-
import datetime
import logging

from django.core.management.base import BaseCommand

from weather.const import SUNRISE_HOUR, SUNSET_HOUR
from weather.models import Place, WebCamPlace

logger = logging.getLogger('update_data')


class Command(BaseCommand):
    def handle(self, *args, **options):
        for place in Place.objects.all():
            try:
                logger.info('Place {}'.format(place))
                place.update()
            except Exception as e:
                logger.exception(e)

        if SUNRISE_HOUR < datetime.datetime.today().hour < SUNSET_HOUR:
            for camera in WebCamPlace.objects.all():
                try:
                    logger.info('WebCam {}'.format(camera))
                    camera.update()
                except Exception as e:
                    logger.exception(e)
