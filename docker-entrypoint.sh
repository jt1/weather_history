#!/bin/bash
./wait-for-it.sh db:5432
python manage.py migrate --settings=docker_settings
python manage.py collectstatic --settings=docker_settings --noinput
python manage.py loaddata initial_data.json --settings=docker_settings
python manage.py update_data --settings=docker_settings
/usr/local/bin/gunicorn wsgi_docker:application -w 2 -b :8000
# python manage.py runserver 0.0.0.0:8000 --settings=docker_settings"

