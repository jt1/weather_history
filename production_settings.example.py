from settings import *

DEBUG = False
ALLOWED_HOSTS = ['weather.example.com']
STATIC_ROOT = '/home/weather_history/static'
DATABASES['default']['NAME'] = '/home/weather_history/db.sqlite3'

AWS_ACCESS_KEY_ID = 'XXX'
AWS_SECRET_ACCESS_KEY = 'YYY'
AWS_STORAGE_BUCKET_NAME = 'bucket'
AWS_S3_REGION_NAME = 'region'
AWS_S3_ENDPOINT_URL = 'endpoint'
AWS_S3_OBJECT_PARAMETERS = {'CacheControl': 'max-age=86400'}
AWS_QUERYSTRING_AUTH = False
AWS_S3_FILE_OVERWRITE = False

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

THUMBNAIL_DEFAULT_STORAGE = DEFAULT_FILE_STORAGE
