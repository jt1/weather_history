# -*- coding: utf-8 -*-

from __future__ import with_statement
from fabric.api import env, local, sudo, run, cd, prefix
from fabric.operations import put

env.hosts = ['a.fundin.pl']
env.user = 'qba'
env.port = 2200

settings = 'fundin_production_settings'


def deploy():
    local('./manage.py test')
    local('./manage.py check')
    with cd('/home/qba/apps/weather_history/'):
        run("git checkout master")
        run("git reset --hard HEAD")
        run("git pull origin master")
        with prefix('source /home/qba/.virtualenvs/weather_history/bin/activate'):
            run("pip install -r requirements.txt")
            put('{}.py'.format(settings), 'production_settings.py')
            run("./manage.py migrate --settings=production_settings")
            run("./manage.py collectstatic --settings=production_settings --noinput")
            run('./manage.py check  --settings=production_settings')
        sudo("service uwsgi restart")
        sudo("service nginx reload")
